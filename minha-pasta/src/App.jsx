import logo from "./logo.svg";
import "./App.css";
import Myname from "./components/Myname";
import Myage from "./components/Myage";
import Color from "./components/Color";
import Count from "./components/Count";

import { BrowserRouter, Routes, Route } from "react-router-dom";

import Home from "./rotas/Home";
import Photos from "./rotas/Photos";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>@joaodiazz</p>

        {/* desafio 6 */}
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/photos" element={<Photos />} />
          </Routes>
        </BrowserRouter>

        <Myname name="João Victor Gomes Dias" />
        <Myage age="21" />

        <Color color="#673" />

        <Count />
      </header>
    </div>
  );
}

export default App;
