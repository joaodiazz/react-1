import { useState } from "react";

const Count = () => {
  const [count, setCount] = useState(0);

  function incrementa(e) {
    e.preventDefault();
    setCount((prev) => {
      return prev + 1;
    });
  }

  return (
    <>
      <h1>{count}</h1>
      <button onClick={incrementa}>incrementar</button>
      <br />
    </>
  );
};

export default Count;
