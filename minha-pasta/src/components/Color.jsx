import "./Color.css";

const Color = ({ color }) => {
  return (
    <div className="quadrado" style={{ backgroundColor: color || "#000" }}>
      <p>passe um valor no atributo color e mude a cor do quadrado</p>
    </div>
  );
};

export default Color;
